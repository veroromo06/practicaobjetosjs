function Persona(nombre, sexo, edad) {
    let persona = Object.create(constructorPersona);
    persona.nombre = nombre;
    persona.sexo = sexo;
    persona.edad = edad;
    return persona;
}

let constructorPersona = {
    datos: function() {
        return `${this.nombre} tiene ${this.edad}`;
    }

}

function Gerente(nombre, sexo, edad, departamento, personas) {
    let gerente = Persona(nombre, sexo, edad);
    Object.setPrototypeOf(gerente, constructorGerente);
    gerente.departamento = departamento;
    gerente.personas = personas;
    return gerente;
}

let constructorGerente = {
    AdministrarDeptos() {
        return `${this.nombre} administra el departamento ${this.departamento}`;
    },
    RealizararNomina() {
        return `${this.nombre} realiza nómina`;
    }
}

function Empleado(nombre, sexo, edad, departamento, posicion) {
    let empleado = Persona(nombre, sexo, edad);
    empleado.departamento = departamento;
    empleado.posicion = posicion;
    return empleado;
}

let constructorEmpleado = {
    realizarInventario() {
        return `${this.nombre} realiza inventario`;
    },
    hacerVenta() {
        return `${this.nombre} hace venta`;
    }

}

// Creación de objetos
let p1 = Gerente("Adrian Gutierrez", "H", 35, "Electrónica", 5);
let p2 = Gerente("María García", "M", 40, "Hogar", 3);
let p3 = Gerente("Marcela Flores", "M", 40, "Blancos", 2);
let p4 = Empleado("Sergio Rodríguez", "H", 35, "Electrónica", "Videojuegos");
let p5 = Empleado("César López", "H", 37, "Electrónica", "Sonido");
let p6 = Empleado("Adriana Ortiz", "M", 30, "Belleza", "Cosméticos");
let p7 = Empleado("Maricela Cruz", "M", 38, "Belleza", "Perfumería");
let p8 = Empleado("Rodrigo Chávez", "H", 35, "Hogar", "Electrodomésticos");
let p9 = Empleado("Cecilia Valdés", "M", 40, "Hogar", "Blancos");

// Enviar a pantalla el contenido de los objetos
document.getElementById("p1").innerHTML = `${p1.nombre} ${p1.sexo} ${p1.edad} ${p1.departamento} ${p1.personas}`;
document.getElementById("p2").innerHTML = `${p2.nombre} ${p2.sexo} ${p2.edad} ${p2.departamento} ${p2.personas}`;
document.getElementById("p3").innerHTML = `${p3.nombre} ${p3.sexo} ${p3.edad} ${p3.departamento} ${p3.personas}`;
document.getElementById("p4").innerHTML = `${p4.nombre} ${p4.sexo} ${p4.edad} ${p4.departamento} ${p4.posicion}`;
document.getElementById("p5").innerHTML = `${p5.nombre} ${p5.sexo} ${p5.edad} ${p5.departamento} ${p5.posicion}`;
document.getElementById("p6").innerHTML = `${p6.nombre} ${p6.sexo} ${p6.edad} ${p6.departamento} ${p6.posicion}`;
document.getElementById("p7").innerHTML = `${p7.nombre} ${p7.sexo} ${p7.edad} ${p7.departamento} ${p7.posicion}`;
document.getElementById("p8").innerHTML = `${p8.nombre} ${p8.sexo} ${p8.edad} ${p8.departamento} ${p8.posicion}`;